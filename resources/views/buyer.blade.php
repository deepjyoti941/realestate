@extends('layouts.property')


@section('content')
    <div id="content" class="col-lg-8 col-md-6 col-sm-6 col-xs-12 clearfix">
        <div class="property_wrapper boxes clearfix">

            <div class="property_desc clearfix" style="padding-left: 10px;">
                <p>A home is probably the biggest financial investment you’ll make in your life. Before you get started,
                    do some homework. This handy Buyer’s Guide will show you some things to keep in mind as you’re
                    hunting for that home of your dreams.</p>

                <div class="qa-message-list" id="wallmessages">
                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Determine How Much You Can Afford</h5>

                                    {{--<div class="post-type">--}}
                                    {{--<p>Project</p>--}}
                                    {{--</div>--}}
                                    {{--<div class="post-time">--}}
                                    {{--<p><i class="glyphicon glyphicon-time"></i> 2 Min Ago</p>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-1"
                                           href="#faq-cat-1-sub-1">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>How much house you can afford is largely dependent on how large a
                                                    mortgage –
                                                    basically, a home loan -- you can handle. Start your research by
                                                    using the simple
                                                    mortgage calculators I have on my website to see whether you can
                                                    afford to pay the
                                                    monthly mortgage on the kinds of houses you have in mind.</p>

                                                <p>You may even apply for a mortgage at a lender before you start
                                                    looking for a home.
                                                    This is called getting pre-qualified for a loan; it will tell you
                                                    exactly how much
                                                    you can afford and may make the closing process go faster.</p>

                                                <p>But, remember that owning a home involves more than a monthly
                                                    mortgage. You’ll also
                                                    have to consider money you’ll need to have at hand when you make an
                                                    offer, when you
                                                    close on a home and on a monthly basis after the home is yours.</p>

                                                <h3>Payments you may have to make when you submit an offer and at
                                                    closing include:</h3>
                                                <ul>
                                                    <li>
                                                        Earnest money, usually 1% to 5% of the cost of the house, which
                                                        you pay as a deposit on the house when you submit your offer.
                                                        It’s your proof that you’re a serious buyer 
                                                        down payment, usually 10% to 20% of the cost of the house, which
                                                        you must pay at closing
                                                    </li>
                                                    <li>
                                                        Mortgage insurance, paid by borrowers making a down payment of
                                                        less than 20%
                                                    </li>
                                                    <li>
                                                        Closing costs, usually 3% to 4% of the cost of the house, to pay
                                                        for processing all the paperwork
                                                    </li>
                                                </ul>
                                                <h3>Don’t forget the day-to-day expenses you may incur once you own that
                                                    home. This includes:</h3>
                                                <ul>
                                                    <li>
                                                        Utilities
                                                    </li>
                                                    <li>
                                                        Homeowner or condo association dues
                                                    </li>
                                                    <li>
                                                        Property taxes
                                                    </li>
                                                    <li>City or County taxes</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Shop for a Home</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-2">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>House hunting can be both exciting and frustrating. Most homebuyers
                                                    see roughly many houses before buying one. To make the search easier
                                                    and faster, nearly half of all house hunters today begin by browsing
                                                    for properties on the Internet, using web sites like this one. 
                                                    Please go to my home page and click on the "Search For Homes" link
                                                    and you will have access to the same data as Realtors in the areas
                                                    from Burbank to Thousand Oaks in Los Angeles County.  For Ventura
                                                    County Property information click on the other link "Here" on my
                                                    homepage.  This information is up-to-date and accurate.  Other sites
                                                    on the internet do not provide up-to-date data and lag behind my
                                                    site</p>

                                                <p>The Internet is a quick way to see whether the houses that are
                                                    currently available meet the following critical criteria: in the
                                                    right location, with the right features and at the right price. If
                                                    you find after your search on my website that few properties meet
                                                    with your expectations, you may want to readjust your criteria –
                                                    change the location, features, price – to increase your chances of
                                                    finding a house that works for you. If you have any difficulties in
                                                    this initial search, feel free to contact me for assistance. Homes
                                                    can become available instantly and I'am always the most current
                                                    resource for literally up to the minute new home listing
                                                    information.</p>

                                                <p>Once you know what you want, where you want it and what you can
                                                    afford, it’s time to see the houses for yourself. To help stay
                                                    focused, bring with you a checklist of things that you’ve decided
                                                    ahead of time are important qualities of your future home.</p>

                                                <h3>This might include:</h3>
                                                <ul>
                                                    <li>Is there enough room for you to grow in?</li>
                                                    <li>Is the house structurally sound?</li>
                                                    <li>Is the house in move-in condition or will it need work?</li>
                                                    <li>Is it close enough to everyday needs, such as grocery stores,
                                                        schools, work?
                                                    </li>
                                                    <li>Will you feel safe here?</li>
                                                    <li>Do the appliances that are part of the sale work?</li>
                                                    <li>Is the yard right for your needs?</li>
                                                    <li>Do you like the floor plan?</li>
                                                    <li>Is there enough storage?</li>
                                                    <li>Will you be happy in this house in winter, summer, spring,
                                                        fall?
                                                    </li>
                                                </ul>
                                                <p>You may also want to take some exterior and interior photos of each
                                                    house you visit so that you can keep track of its pros and cons.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Find a Real Estate Professional...That's were I come in...</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-3">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>While you’re not required to use a real estate professional, it is a
                                                    good idea. A professional has access to a network of contacts and
                                                    can draw from extensive market knowledge to help pinpoint the right
                                                    house for you quickly.</p>

                                                <p>A professional also can help you structure your deal to save money,
                                                    explain the advantages and disadvantages of different types of
                                                    mortgages and guide you through the paperwork.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Research Different Mortgages</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-4">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>There are a variety of mortgage types available today, each with
                                                    advantages and disadvantages depending on how long you plan to live
                                                    in the home, the financial marketplace and your income potential,
                                                    among other things.</p>

                                                <p>A fixed-rate mortgage is the most common. In a fixed-rate mortgage,
                                                    your interest rate and payment stay the same for the life of the
                                                    loan.</p>

                                                <p>An adjustable-rate mortgage usually starts out at lower interest
                                                    rates and lower monthly payments than fixed-rate mortgages, but your
                                                    rate and monthly payments may rise and fall based on a financial
                                                    index.</p>

                                                <p>
                                                    There are also several government mortgage programs available,
                                                    including FHA mortgages, which are designed to help people who might
                                                    not otherwise qualify for a loan.
                                                </p>

                                                <p>
                                                    You may also have a choice in loan terms. There are 30-year loans
                                                    and 15-year loans.
                                                </p>

                                                <p>
                                                    It’s best to talk to me about your best mortgage option.  I may
                                                    refer you to a Mortgage Broker that can discuss current market
                                                    financing packages and provide a FREE Loan Qualification.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Make an Offer</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-5">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>When you’ve found a house you really want, it’s time to make the
                                                    offer. How much you offer may depend on a number of factors:</p>
                                                <ul>
                                                    <li>Is the asking price fair? Here’s where the legwork you put in
                                                        while shopping for a home pays off. Decide whether this house is
                                                        priced right or out of line in the current marketplace.
                                                    </li>
                                                    <li>Is the house in good condition? Is this house in move-in
                                                        condition or will it need a lot of work? Take any costs of
                                                        improvement into consideration when deciding your offer price.
                                                    </li>
                                                    <li>Has it been on the market long? Usually the longer a house has
                                                        been on the market, the more likely it is the owner would accept
                                                        a lower offer. Or maybe it’s just overpriced for the market.
                                                    </li>
                                                    <li>Is it a seller’s or buyer’s market? If the houses you’re
                                                        interested in are being bought as soon as they’re listed, that
                                                        means you’ve got a lot of competition from other buyers; offer
                                                        accordingly. If houses aren’t selling fast, you may have more
                                                        leverage in negotiating a lower price.
                                                        Once you’ve determined how much you’d like to offer, work with
                                                        your real estate professional to submit the proper information.
                                                        This includes:
                                                        <ul>
                                                            <li>A complete, legal description of the house</li>
                                                            <li>The amount of earnest money you’re paying</li>
                                                            <li>The down payment and financing details</li>
                                                            <li>A proposed move-in date</li>
                                                            <li>The price you’re offering</li>
                                                            <li>A proposed closing date</li>
                                                            <li>The length of time your offer is valid</li>
                                                            <li>Details of the deal</li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <p>This can be just the beginning of the negotiation process. The seller
                                                    has three options: accept your offer, counter your offer or reject
                                                    your offer. Let me advise you on the best way to present your offer
                                                    for a good outcome.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Begin Contingency Period</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-6">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>When your offer has been accepted, the contingency period begins.
                                                    This is time that allows you to obtain financing, perform
                                                    inspections and satisfy any other contingencies of your purchase
                                                    agreement.</p>

                                                <p>Obtaining financing might include loan approval, which will include
                                                    an appraisal of the property. Also be prepared to make your down
                                                    payment, which is usually due several days before the close of
                                                    escrow.</p>

                                                <p>Now is the time to schedule a professional inspection of the
                                                    property; it is one of the best safeguards you can take before
                                                    buying. A home inspector should check (and may give you a rough
                                                    price for repairs on) the electrical system, plumbing and waste
                                                    disposal, the water heater, insulation and ventilation, water source
                                                    and quality, pests, foundation, doors, windows, ceilings, walls,
                                                    floors and roof.</p>

                                                <p>Keep in mind that the inspector isn’t there to tell you whether
                                                    you’re getting a good deal. He or she is there to give you an
                                                    educated opinion on whether the house is structurally and
                                                    mechanically sound and fill you in on any repairs that are
                                                    needed.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Buy Homeowner’s Insurance</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-7">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>A paid homeowner’s insurance policy is required at closing. I will
                                                    help make sure your insurance company and your title officer are
                                                    working together to put your policy in effect by the close of
                                                    escrow. But, if you get your insurance agent involved early in your
                                                    home-buying process, he or she may also help point out ways to help
                                                    keep your insurance premiums lower.</p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Complete Settlement or Closing</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-8">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-8" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>When the property you’re buying has been inspected and you’ve had
                                                    your final walk-through of the property to see that all contingency
                                                    conditions – such as final repairs made by the seller -- have been
                                                    met, it’s time to face the paperwork. You will be signing loan
                                                    documents and closing papers, paying the balance of your down
                                                    payment and closing costs. This is the day you get the keys to your
                                                    new home. Congratulations!</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- end property_wrapper -->

    </div><!-- end content -->
@stop


