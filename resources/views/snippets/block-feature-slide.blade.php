<ul class="slides">
    {{--@forelse( $slidePosts as $post )--}}
    {{-- Traditionally include the variable file so that it could be shared to this view --}}
    <?php /*include base_path() .'/resources/views/snippets/variables-property.blade.php'; */ ?>
    {{--<li>--}}
    {{--<div class="desc">--}}
    {{--<div class="ps-desc">--}}
    {{--<h3><a href="{{ $url }}">{{ $title }}</a></h3>--}}
    {{--<p>{{ $excerpt }} <a href="{{ $url }}">read more</a></p>--}}
    {{--<span class="price">{{ $price }}</span>--}}
    {{--@if( $propertyType )--}}
    {{--<a href="#" class="status">{{ $propertyType }}</a>--}}
    {{--@endif--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<a href="{{ $url }}">{!! $featureImage !!}</a>--}}
    {{--</li>--}}
    {{--@empty--}}
    {{--@endforelse--}}


    <li>
        <div class="desc">
            <div class="ps-desc">
                <h3>Flat Fee</h3>

                <p>Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee <a href="{{ url()}}/static/about-us">read more</a></p>
            </div>
        </div>
        <a href="">
            <img src="{{ asset('realestate/images/new_banners/banner2.png') }}"
                 class="post-feature-image img-responsive">
        </a>
    </li>
    <li>
        <div class="desc">
            <div class="ps-desc">
                <h3>Flat Fee</h3>

                <p>Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee <a href="{{ url()}}/static/about-us">read more</a></p>
            </div>
        </div>
        <a href="">
            <img src="{{ asset('realestate/images/new_banners/banner1.png') }}"
                 class="post-feature-image img-responsive">
        </a>
    </li>
    <li>
        <div class="desc">
            <div class="ps-desc">
                <h3>Flat Fee</h3>

                <p>Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee <a href="{{ url()}}/static/about-us">read more</a></p>
            </div>
        </div>
        <a href="">
            <img src="{{ asset('realestate/images/new_banners/banner4.png') }}"
                 class="post-feature-image img-responsive">
        </a>
    </li>
    <li>
        <div class="desc">
            <div class="ps-desc">
                <h3>Flat Fee</h3>

                <p>Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee <a href="{{ url()}}/static/about-us">read more</a></p>
            </div>
        </div>
        <a href="">
            <img src="{{ asset('realestate/images/new_banners/banner5.png') }}"
                 class="post-feature-image img-responsive">
        </a>
    </li>
    <li>
        <div class="desc">
            <div class="ps-desc">
                <h3>Flat Fee</h3>

                <p>Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee <a href="{{ url()}}/static/about-us">read more</a></p>
            </div>
        </div>
        <a href="">
            <img src="{{ asset('realestate/images/new_banners/banner6.png') }}"
                 class="post-feature-image img-responsive">
        </a>
    </li>
    <li>
        <div class="desc">
            <div class="ps-desc">
                <h3>Flat Fee</h3>

                <p>Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee <a href="{{ url()}}/static/about-us">read more</a></p>
            </div>
        </div>
        <a href="">
            <img src="{{ asset('realestate/images/new_banners/banner3.png') }}"
                 class="post-feature-image img-responsive">
        </a>
    </li>
</ul><!-- end slides -->