@extends('layouts.property')


@section('content')
    <div id="content" class="col-lg-8 col-md-6 col-sm-6 col-xs-12 clearfix">
        <div class="property_wrapper boxes clearfix">
            <div class="property_desc clearfix" style="padding-left: 10px;">
                <div class="row">
                    <div class="col-md-4">
                        <address>
                            <strong>Investors group.</strong><br>
                            behind Vijay punjab hotel,<br>
                            bhaaji market-kharigaon, <br>
                            B P cross Road,<br>
                            Bhayandar (East)<br>
                            Mumbai- 401105<br>
                            <abbr title="Phone">P:</abbr> 022-28049292 <br>
                            <abbr title="Phone">M:</abbr> 91-8655444900
                        </address>
                        <address>
                            <a href="mailto:#">contact@fixfees.com</a>
                        </address>
                    </div>
                    <div class="col-md-8">
                        <img width="600" src="http://maps.googleapis.com/maps/api/staticmap?center=Investors+group,+behind+Vijay+punjab+hotel,+bhaaji+market-kharigaon,+B+P+cross+Road,+Bhayandar,+Mumbai&zoom=16&scale=1&size=600x400&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0x2382c4%7Clabel:%7CInvestors+group,+behind+Vijay+punjab+hotel,+bhaaji+market-kharigaon,+B+P+cross+Road,+Bhayandar,+Mumbai" alt="Google Map of Investors group, behind Vijay punjab hotel, bhaaji market-kharigaon, B P cross Road, Bhayandar, Mumbai">                    </div>
                </div>
            </div>


        </div>

    </div><!-- end content -->
@stop


