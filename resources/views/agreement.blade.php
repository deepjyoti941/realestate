@extends('layouts.property')


@section('content')
    <div id="content" class="col-lg-8 col-md-6 col-sm-6 col-xs-12 clearfix">
        <div class="property_wrapper boxes clearfix">
            <div class="title clearfix">
                <h3>Terms and conditions</h3>
            </div>

            <div class="property_desc clearfix" style="padding-left: 10px;">

                <ol>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">A fixed
                            amount will be charged as service charges for the property sold thru
                            us.(which is less than the brokerages being charged by the property
                            dealers/estate agents.) Documentation charges, govt. taxes and
                            levies will be extra, as applicable.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">All the
                            payments are to be made thru cheque/or bank transfer. No Cash
                            payments will be accepted.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">50% of total
                            service charges will have to be paid in advance.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">This advance
                            will be adjusted against the services provided to the seller.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">The
                            agreement will come to end when the deal is concluded with handing
                            over of the property to the buyer.
                        </p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">Our services
                            will start immediately on signing the agreement and receipt of 50%
                            advance of the total deal value.
                        </p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">The deal has
                            to be concluded within 180 days starting from the date of agreement
                            with us. The duration can be extended in case of genuine issues and
                            at the discretion of both the party with or without additional
                            charges.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">If for any
                            reasons, the deal does not take place, then part of the advance
                            amount collected by us will be refunded after deduction of expenses
                            incurred till date.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">The seller
                            needs to provide all the necessary documents like Flat sell
                            agreement(chain of agreements), Copy of PAN card and Aadhar Card,
                            Share certificate, NOC from Housing Society, NOC from bank(in case
                            there is any unpaid loans.), Maintenance , Electric bills prior to
                            listing of the property for sale and also provide any other
                            documents as requested by buyer.
                        </p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">During the
                            agreement between seller and us in force, The seller is allowed to
                            deal with other parties/companies. But the seller has to pay us the
                            agreed fixed amount mentioned in the agreement between us and the
                            seller. (It means that the seller is free to sell their property
                            directly/or through third party with no liabilities attached to us. The
                            seller has to pay us the fixed amount as mentioned in the agreement.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%"><span style="color: rgb(99, 99, 99);">
	Either party is free to terminate the agreement. In case either
	party wish to terminate agreement, we will refund the advance amount
	received from seller after deducting the incidental charges incurred
	by us till date.</span></p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%"> Our
                            liability is limited to the maximum of the total fees collected from
                            the seller.
                        </p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">Management
                            reserves the right to amend the terms and conditions without prior
                            notice.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">We will
                            provide the following services:-</p>
                    </li>
                </ol>
                <ul>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">Our legal
                            team will verify the property along with all the required documents
                            to their complete satisfaction and then only the property will be
                            listed for sell.
                        </p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">Seller will
                            be provided adequate exposure to the prospects thru advertisement in
                            news paper, our web portal, SMS, Brochure, and direct selling
                            agents.</p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">Seller will
                            be provided with the direct contacts of the buyer.
                        </p>
                    </li>
                    <li><p style="margin-bottom: 0.35cm; line-height: 115%">Encourage
                            Seller&amp; buyers to interact directly with each other so as to
                            maintain total transparency in the said deal. We will acts as
                            facilitator between them.</p>
                    </li>
                </ul>
                <p style="margin-left: 3.81cm; margin-bottom: 0.35cm; line-height: 115%">
                    <br><br>
                </p>


            </div>


        </div>
        <!-- end property_wrapper -->

    </div><!-- end content -->
@stop


