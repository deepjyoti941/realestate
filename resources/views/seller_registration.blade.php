@extends('layouts.property')


@section('content')
    <div id="content" class="col-lg-8 col-md-6 col-sm-6 col-xs-12 clearfix">
        <div class="property_wrapper boxes clearfix">
            <div class="property_desc clearfix" style="padding-left: 10px;">
                <h4>Seller registration page</h4>
                @if(Session::has('errors'))
                    <div class="alert alert-warning">
                        @foreach(Session::get('errors')->all() as $error_message)
                            <p>{{ $error_message }}</p>
                        @endforeach
                    </div>
                @endif

                <form id="contact" method="post" class="form" role="form" action="{{ URL::to('/seller-store') }}">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <h5>Contact Details</h5>

                    <div class="row">
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="name" name="name" placeholder="Name" type="text"
                                   autofocus="">
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="email" name="email" placeholder="Email" type="text">
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="phone" name="phone" placeholder="Phone Number" type="text"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4 col-md-4 form-group">
                            <textarea class="form-control" id="streetAddress" name="streetAddress"
                                      placeholder="Street Address" rows="1"></textarea>
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="city" name="city" placeholder="City" type="text">
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="state" name="state" placeholder="State" type="text">
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="pin" name="pin" placeholder="pin code" type="text"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        </div>
                    </div>
                    <h5>Property Information</h5>

                    <div class="row">
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" type="text" name="bathrooms" placeholder="no. of bathrooms"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" type="text" name="bedrooms" placeholder="no. of bedrooms"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        </div>
                        <div class="col-xs-4 col-md-4 form-group">
                            <input class="form-control" id="sqft" name="sqft" placeholder="Approx. Sq ft." type="text"
                                   onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-4 col-md-4 form-group">
                            <select class="form-control" name="propertyType">
                                <option value="0">--select property type--</option>
                                <option value="Food Industry">Food Industry</option>
                                <option value="Beauty Salons">Beauty Salons</option>
                                <option value="Commercial Property">Commercial Property</option>
                                <option value="Investor Required">Investor Required</option>
                                <option value="Manufacturing">Manufacturing</option>
                                <option value="Leisure, Travel & Tourism"> Leisure, Travel & Tourism</option>
                                <option value="Hotels">Hotels</option>
                                <option value="Night Clubs">Night Clubs</option>
                                <option value="Laundry & Dry Cleaning">Laundry & Dry Cleaning</option>
                                <option value="Retail & Shops">Retail & Shops</option>
                                <option value="Banks & Exchangers">Banks & Exchangers</option>
                                <option value="Education & Training">Education & Training</option>
                                <option value="Construction">Construction</option>
                                <option value="Health Care & Pharmacy">Health Care & Pharmacy</option>
                                <option value="Business Services">Business Services</option>
                                <option value="General Trading">General Trading</option>
                                <option value="IT-Companies">IT-Companies</option>
                                <option value="Transport & Auto Service">Transport & Auto Service</option>
                                <option value="Others">Others</option>
                            </select>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <select class="form-control" name="parkinOption">
                                <option value="0">--car parking space--</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <div class="col-xs-4 col-md-4">
                            <div class='input-group date' id='datetimepicker1'>
                                <input type='text' class="form-control" name="saleDate" placeholder="When to sell (specify date)"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-md-12 form-group">
                            <textarea class="form-control" id="comments" name="comments" placeholder="comments if any"
                                      rows="5"></textarea>
                        </div>
                        <div class="col-xs-12 col-md-12 form-group">
                            <button class="btn btn-primary pull-right" type="submit">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
            <br/><br/>
            <hr>
            <div class="property_desc clearfix" style="padding-left: 10px;">
                <p>whether you’re planning to sell your home in the next few months or just studying up for that
                    eventuality, there’s no time like now to prepare. If buying a house seems complicated, selling
                    involves even more responsibilities and expenses.</p>
                <h5><strong>Here are some common steps to selling your home:</strong></h5>

                <div class="qa-message-list" id="wallmessages">
                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Prepare Your Home for Sale</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-1"
                                           href="#faq-cat-1-sub-1">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-1" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>Well before you’re ready to plant that “For Sale” sign in your front
                                                    yard, there is work to be done to prepare your home for sale.</p>

                                                <p>Remember how keen your eye was to every small detail and defect in
                                                    the houses you saw as a buyer? Now that door to your bedroom that
                                                    never quite closed properly or that leaky faucet that you never got
                                                    around to fixing will be seen by a potential buyer with that same
                                                    keen eye.</p>

                                                <p>Start making the obvious repairs today – even if you don’t plan to
                                                    sell until a year from now. These repairs can cost money and take
                                                    time. Plus fixing it now will allow you to enjoy the results before
                                                    it’s time to move out.</p>

                                                <p>
                                                    If you plan on doing some improvements before the sale, the best
                                                    place to start is where the buyers start: at your curb. Potential
                                                    buyers base a large part of their decision on a property’s “curb
                                                    appeal,” so make yours say something positive. That means a tidy
                                                    front yard, a house with well-painted trim, a tidy driveway and a
                                                    clear, welcoming entryway.
                                                </p>

                                                <p>
                                                    Inside, the biggest return on your investment continues to be
                                                    improvements to the kitchen, followed closely by improvements to the
                                                    master bedroom. If you’re making these improvements shortly before
                                                    selling the house, consider painting and decorating the rooms in
                                                    neutral colors, the most appealing choice to the greatest number of
                                                    potential buyers.
                                                </p>

                                                <p>
                                                    Inside and outside, start reducing the clutter. When it comes time
                                                    to show your home, less will mean more. Potential buyers don’t want
                                                    to see how your closets overflow with clothes, how every room feels
                                                    cramped with furniture, or how the yard is difficult to maneuver
                                                    with that rusty swing set in the way. So downsize now; it not only
                                                    will make the preparation for showing your home easier, it also will
                                                    make packing for your move faster.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Find a Real Estate Professional</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-2">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-2" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>If you’ve been through the home-buying process, you already know how
                                                    complicated the real estate business can be. While you can opt to
                                                    sell your home yourself, it can be time-consuming and often not
                                                    worth the money saved on commissions.</p>

                                                <p>However, if you do hire a real estate professional as your selling
                                                    agent, do your homework. Ask friends and family for recommendations,
                                                    interview several candidates, attend a few open houses and watch the
                                                    professional in action. Do you think this person would present your
                                                    house well to potential clients? </p>

                                                <p>When interviewing me or another candidate, ask him or her to prepare
                                                    a “comparative marketing analysis” for your house. This might
                                                    include a demographic of the neighborhood, the quality of schools in
                                                    the area and a suggested list price for the property.</p>

                                                <p>If you’ve chosen me or another real estate professional to help sell
                                                    your home, you’ll have to sign a contract stating that you’ll work
                                                    solely with them for a designated number of months, often between
                                                    one and six months. This means no other real estate professional
                                                    will be allowed to sell your home on your behalf during this
                                                    time.</p>

                                                <p>So put some thought into the professional you choose and if you
                                                    decide to choose me, I will help you sell your home to a qualified
                                                    buyer for the highest market price in the quickest, most convienent
                                                    timeline.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Get Your Paperwork Together</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-3">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-3" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>After you sign the Listing Agreement with me she will need a lot of
                                                    documents from you to prepare your home for sale. Among the things
                                                    she will want to see are:
                                                <ul>
                                                    <li>Pay-off Notice: A letter to the lender stating intention to
                                                        payoff the mortgage.
                                                    </li>
                                                    <li>Assessments or Easements: If there’s a tax assessment or
                                                        easement on the property, documents stating such will have to be
                                                        included in the purchase contract.
                                                    </li>
                                                    <li>Property Taxes: Proof of property taxes paid.</li>
                                                    <li>Utilities: Provide a record of the past 12 months’ utility
                                                        bills.
                                                    </li>
                                                </ul>
                                                </p>

                                                <p>
                                                    You’ll want to make it clear now which items in the home you want to
                                                    take with you – the heirloom chandelier in the dining room, the
                                                    washer and dryer set you just bought last month – and which can stay
                                                    behind as part of the home sale. I can help show you which items you
                                                    should put away or replace before your house goes on the market.
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Price Your Home</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-4">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-4" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>There are a number of factors that will affect the success of your
                                                    home sale. They include: location of the home, interest rates,
                                                    economic conditions, time of year, condition of the home, marketing
                                                    the home, terms of the sale and accessibility to the home.</p>

                                                <p>Some of these are not within your or my control – location of the
                                                    home, interest rates, economic conditions. The other factors are
                                                    items you should discuss with me to determine what would benefit the
                                                    sale of this property most.</p>

                                                <p>For example, marketing your property in more innovative ways, such as
                                                    on an Internet site like this one, may broaden the pool of potential
                                                    buyers. If you can, waiting for a good time to sell your home –
                                                    spring or fall, the most popularly home buying times – also may help
                                                    it sell faster except in this Seller's market, anytime is a good
                                                    time. And pricing the home properly can make a huge difference in
                                                    whether a house is snapped up within the first several weeks of
                                                    listing or sits on the market for more than a year.</p>

                                                <p>
                                                    To price a home properly, you and I will have to study the local
                                                    market, research comparable properties and consider current market
                                                    conditions. This is where the “comparative marketing analysis” you
                                                    requested when interviewing for a listing agent will come in handy
                                                    as a place to start.
                                                </p>

                                                <p>
                                                    Now check around your neighborhood, your newspaper and Internet
                                                    sites like this one for:
                                                <ul>
                                                    <li>Your competition: Are there many properties just like yours for
                                                        sale in your area right now?
                                                    </li>
                                                    <li>Listing prices: What are other properties like yours listing
                                                        for?
                                                    </li>
                                                    <li>Selling prices: What are other properties like yours selling
                                                        for?
                                                    </li>
                                                </ul>
                                                </p>

                                                <p>
                                                    Based on these findings, I have the experience to help you price
                                                    your property at the right price for a sale that benefits you.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Market Your Home</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-5">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-5" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>Products that sell well usually have a good marketing strategy. The
                                                    same can be said for your home. Work with me to decide where you
                                                    want to advertise. Will the house be advertised only with a yard
                                                    sign? Do you want your house listed for sale not only in newspapers
                                                    but also on Internet sites like this one? When can you make your
                                                    house available for an “open house” showing?</p>

                                                <p>When a potential buyer arrives for an “open house” or drives by and
                                                    sees the for sale sign, you’ll want to provide a home profile
                                                    handout that they can take with them. Decide what information should
                                                    be included in the description of your home that will make it a
                                                    must-see – and hopefully, a must-buy. Include one or more photos of
                                                    the home to showcase the most appealing features of your property
                                                    and help remind potential buyers of what they saw as they visit home
                                                    after home.</p>

                                                <p>
                                                    You may even want to include a few lines about benefits of moving to
                                                    this property, such as good schools, convenience to mass transit and
                                                    other desirable community features.  You'll work with me to discuss
                                                    what the features and benefits of your property are.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Prepare Your Home for Showing (Your Job!)</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-6">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-6" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>Now that you've decided with me on the market price and how you will
                                                    market your home now there’s little time left to get your house
                                                    ready for visitors.</p>

                                                <p>Now is the time to put on the finishing touches, just like that quick
                                                    housecleaning you do before company comes over for dinner.</p>

                                                <p>Outside: Keep your lawn trimmed, the rose bushes pruned, the weeds
                                                    tamed. Put away the garden hose and the tools. Make sure the bulbs
                                                    in your home’s exterior lighting fixtures are all in working order.
                                                    Be vigilant about removing flyers, handouts and newspapers left on
                                                    your front doorstep or driveway.</p>

                                                <p>Inside: Brighten the rooms by opening the drapes, turning on the
                                                    lights, cleaning the windows. Clear the clutter on the kitchen
                                                    counter, bathroom sink, coffee table and couches. Make all the beds.
                                                    Clean all your bathroom and kitchen fixtures. Do a quick vacuuming
                                                    of the entire house, being sure to catch any cobwebs in the corners
                                                    along the ceiling. Finally, take out the garbage.</p>

                                                <p>
                                                    If you have pets, find a safe place to keep them during a house
                                                    showing: in the garage, in the basement or at a friend’s house.
                                                </p>

                                                <p>
                                                    Now leave the work to me. Try to be away from home during a showing,
                                                    but if you happen to be home when the potential buyers arrive, greet
                                                    them at the door then politely excuse yourself. Make yourself scarce
                                                    or go take a walk. It’s easier for a buyer to picture himself or
                                                    herself living in the house when you’re not there. This is your
                                                    home’s time to shine.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Respond to an Offer</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-7">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-7" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>Depending on market conditions, you may receive one or more offers
                                                    for your property from interested buyers. Each offer will include
                                                    the proposed offer price, proposed closing date, proposed move-in
                                                    date, financing, and contingencies that may include an appraisal or
                                                    sale of the buyers’ current home. Let me help you sort through the
                                                    variables to determine whether you should accept, counter-offer or
                                                    reject the offer.</p>

                                                <p>
                                                    If there are multiple offers, each offer will be presented to you in
                                                    the order registered. You don’t need to decide anything until after
                                                    you’ve seen all the offers. If you do accept or counter more than
                                                    one offer, you are required to establish an order of precedence
                                                    noting which is the primary offer, followed by the backups in order.
                                                    This will help you avoid selling the house to more than one buyer.
                                                </p>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="message-item" id="m16">
                        <div class="message-inner">
                            <div class="message-head clearfix">
                                <div class="message-icon pull-left"><a href="#"><i
                                                class="glyphicon glyphicon-check"></i></a></div>
                                <div class="user-detail">
                                    <h5 class="handle">Complete the Settlement</h5>
                                </div>
                            </div>
                            <div class="panel-group" id="accordion-cat-1">
                                <div class="panel panel-default panel-faq">
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-cat-2"
                                           href="#faq-cat-1-sub-8">
                                            <h4 class="panel-title">
                                                Read More
                                                <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span>
                                            </h4>
                                        </a>
                                    </div>
                                    <div id="faq-cat-1-sub-8" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="qa-message-content">
                                                <p>Once you have accepted an offer to buy your house, expect to make
                                                    your house available to a housing inspector, a termite inspector, an
                                                    appraiser and other inspectors. After seeing the results of the
                                                    inspections, the buyer may request additional work is completed
                                                    before purchase, such as repairing a damaged roof or fixing a leaky
                                                    faucet. You should consult with me to determine whether to comply
                                                    with the buyer’s request or risk losing this offer.</p>

                                                <p>
                                                    During this flurry of activity, try to keep your home in show
                                                    condition. The deal has not closed and still may fall through, which
                                                    may mean showing your home to more potential buyers.
                                                </p>

                                                <p>
                                                    In the meantime, the buyer is working with a lender to secure a loan
                                                    for the purchase. When the buyer has written loan approval, a
                                                    closing date can be set.
                                                </p>

                                                <p>
                                                    There will be a final walk-through before all signatures are
                                                    collected and the deal considered done. The buyer will go room by
                                                    room to check that everything is in working condition and, if you
                                                    had agreed to do so, any additional work requested after inspection
                                                    is completed.
                                                </p>

                                                <p>
                                                    Now you can prepare for your own move, notify your utility companies
                                                    of the date to transfer your account to a new address and start
                                                    packing. Congratulations, you’ve sold your home!
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div><!-- end content -->
@stop
