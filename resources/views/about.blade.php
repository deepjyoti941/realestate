@extends('layouts.property')


@section('content')
    <div id="content" class="col-lg-8 col-md-6 col-sm-6 col-xs-12 clearfix">
        <div class="property_wrapper boxes clearfix">
            <div class="property_desc clearfix" style="padding-left: 10px;">
                <p>
                    In a real estate transaction, the relationship between real estate and law is quite close, with
                    aspects of one interwoven with the other. The concept "simple real estate" no longer exists in
                    today's complex real estate and legal worlds. Even the most straightforward of transactions must be
                    carefully crafted to avoid current and future loss due to contract terms neglect.
                </p>

                <p>
                    Estate Realty Group is uniquely qualified to deal with both real estate and its attendant legal
                    issues. In addition to real estate professionals working with attorneys specializing in trusts,
                    probates and family law matters, we provide close coordination with financial planners, Certified
                    Accountants and mortgage professionals to ensure you receive a comprehensive view of your real
                    estate transaction.
                </p>
                <h5><strong>Flat Fee</strong></h5>

                <p>
                    Flat Fee is a new independent real estate portal with a fresh approach to selling real estate - we
                    offer to sell your property for a low, fixed flat fee.  Our fee is fixed for those who appreciate
                    the service and expertise of a real estate professional but like to save money. Unlike traditional
                    real estate agents we do not charge a commission based on a percentage of the sale price of your
                    property.
                </p>

                <p>
                    In addition to extensive "traditional" personalized real estate services, Estate Realty Group,
                    through its association with attorneys specializing in real estate and associated fields, offers a
                    particular expertise for our clients in real estate transactions which arise out of probate, trust
                    estate and family law matters. Our flat-fee realty service approach is to elevate the future of real
                    estate.
                </p>

                <h5><strong>How Your Property is Advertised and Marketed</strong></h5>

                <p>
                    Estate Realty Group employs all the traditional methods of advertising property listings: Signage,
                    On Site Flyers, Broker Open House, Public Open Houses, Neighborhood Flyers and Professional
                    Networking. In addition, listings are advertised through the appropriate regional Multiple media
                    Services and are also featured on the Internet through websites. Our agents represent over fifty
                    years of experience in all types of real estate: residential, commercial and investment. We have
                    professional working relationships with brokers to better serve the needs of our clients.
                </p>
            </div>


        </div>

    </div><!-- end content -->
@stop


