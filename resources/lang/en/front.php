<?php
   /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
return [
	'index' => 'Home',
	'properties' => 'Properties',
    'buyers' => 'Buyers',
    'sellers' => 'Sellers',
    'contact-us' => 'Contact Us',
	'property' => 'Property',
	'error-404' => 'Page 404 Error',
	'search' => [
		'index' => 'Search',
		'advance' => 'Advance Search'
	],
    'user' => [
        'properties' => [
            'index' => 'List of Properties',
            'create' => 'Create Property Post',
            'edit' => 'Edit Property'
        ]
    ],
    'agreement-list' => 'Agreement Policies',
    'how-seller-work' => 'How seller work',
    'how-buyer-work' => 'How buyer work',
    'about-us' => 'About Us'
];