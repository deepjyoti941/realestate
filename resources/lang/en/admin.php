<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'root' => 'Dashboard',
    'menu'  => [
        'index' => 'Menu'
    ],    
    'files' => [
        'index' => 'Files'
    ],
    'users'  => [
        'index' => 'Users',
        'add' => 'Add New User',
        'create' => 'Create New User',
        'register' => 'Register New User',
        'edit' => 'Edit User'
    ],
    'posts'  => [ 
        'all' => 'All Properties',
        'index' => 'Property',
        'create' => 'Create Property',
        'add' => 'Add New Property',
        'edit' => 'Edit Property',
        'store' => 'Save',
        'blog_title' => 'Title',
        'blog_content' => 'Content'       
    ],
    'pages' => [
        'all' => 'All Pages',  
        'index' => 'Pages',
        'create' => 'Create Page',
        'add' => 'Add New Page',
        'edit' => 'Edit Page',
        'store' => 'Save',
        'blog_title' => 'Title',
        'blog_content' => 'Content'
    ],
    'auth' => [
        'login' => 'Login',
    ],
    'categories' => [
        'title' => 'Categories'
    ],
    'tags' => [
        'index' => 'Tags',
        'title' => 'Tags',
        'save' => 'Save Tag',
        'add-block' => 'Add New Category/Tag',
        'list' => 'List of Categories/Tags'
    ],
    'image' => [
        'feature' => 'Feature Image',
    ],
    'post-media' => [
        'index' => 'Add Media Files'
    ],
    'post-subsidiary-media' => [
        'index' => 'Add Subsidiary Media Files'
    ],
    'post_category' => 'Property Category',
    'properties' => 'Properties',
    'geocomplete_title' => 'Location Search',
    'property_details' => 'Property Details',
    'property_area' => 'Area (m2)',
    'property_address' => 'Address',
    'property_price' => 'Price',
    'property_type' => 'Type',
    'property_status' => 'Status',
    'property_country' => 'Country',
    'property_location' => 'Location'
];
