<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class FrontMenu
{
    public function handle($request, Closure $next)
    {
        Menu::make('menu-front', function($menu) {
            $home = $menu->add('Home', route('front.index') )
                ->icon('fa fa-home', 'fa')
                ->prependIcon();

            $properties = $menu->add( trans( 'front.properties' ) , route('front.properties') )
                ->prependIcon();

            $buyers = $menu->add( trans( 'front.buyers' ) , route('front.buyers') )
                ->prependIcon();

            $sellers = $menu->add( trans( 'front.sellers' ) , route('front.sellers') )
                ->prependIcon();

            $aboutUs = $menu->add( trans( 'front.about-us' ) , route('front.about-us') )
                ->prependIcon();

            $contactUs = $menu->add( trans( 'front.contact-us' ) , route('front.contact-us') )
                ->prependIcon();
        });

        return $next($request);
    }
}
