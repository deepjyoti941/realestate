$(function () {

    var origin = document.location.origin;
    var currentUrl = document.URL;
    var urlArray = currentUrl.split('/');
    var propertyId = urlArray[urlArray.length - 2];

    $('#flash-overlay-modal').modal();

    $('.remove-subsidiary-image').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var data = {
            propertyId: propertyId,
            imageId: $this.val(),
            '_token': $('input[name=_token]').val()
        };
        console.log(data);
        swal({
            title: "Delete subsidiary image",
            text: "Are you sure you wana delete!",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            $.ajax({
                url: origin + '/admin/delete-subsidiary-media',
                type: 'POST',
                data: data,
                success: function (data) {
                    console.log(data);
                    if (data.status) {
                        swal("Media deleted successfully!");
                        $this.parent().parent().fadeOut();
                    } else swal("Some error occurred while deleting media!");
                }
            });
        });
    });

    $('#check-all').click(function (event) {
        if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function () {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function () {
                this.checked = false;
            });
        }

    });


    var files = [];
    $('.btn-add-selected').click(function () {

        var propertyId = getParameterByName('post');
        $('.tables-files > tbody').find('tr').each(function () {
            var row = $(this);
            var imageId = row.find('input[type="checkbox"]').attr('data-id');
            if (!row.find('input[type="checkbox"]').is(':checked')) {

            } else {
                if ($.inArray(imageId, files) == -1)
                    files.push(imageId);
            }
        });

        if (files.length == 0) {
            sweetAlert("Oops...", "you must select some files!", "error");
        } else {
            var data = {
                propertyId: propertyId,
                files: files,
                type: 'bulkUpload',
                '_token': $('input[name=_token]').val()
            }
            $.ajax({
                url: origin + '/admin/post-subsidiary-media-store',
                type: 'POST',
                data: data,
                success: function (data) {
                    if (data.status) {
                        swal("Media uploaded successfully!");
                        setTimeout(function () {
                            window.location.replace(origin + '/admin/posts/'+propertyId+'/edit')
                        }, 2000);
                    } else swal("Some error occurred while deleting media!");
                }
            });
        }
    });

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
});