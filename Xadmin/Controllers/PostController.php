<?php

namespace Xadmin\Controllers;

use Illuminate\Http\Request;
use App\Http\PostRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;
use Xadmin\Models\Post;
use Xadmin\Models\PostTag;
use Xadmin\Models\FileMedia;
use Xadmin\Models\PostMeta;
use Xadmin\Repositories\PostRepo;
use Xadmin\Repositories\PostSubsidiaryMediaRepo;

class PostController extends Controller
{

    protected $postRepo;
    protected $postSubsidiaryMediaRepo;

    public function __construct(PostRepo $postRepo, PostSubsidiaryMediaRepo $postSubsidiaryMediaRepo)
    {
        $this->postRepo = $postRepo;
        $this->postSubsidiaryMediaRepo = $postSubsidiaryMediaRepo;
    }

    public function index()
    {
        $posts = Post::where('post_type', 'post')->paginate(10);
        return view('cms::post.posts', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $post = new Post();

        return view('cms::post.post', compact('post'));
    }


    public function store(Request $request)
    {
        // Save new post
        $post = $this->postRepo->create($request);

        Flash::overlay('property successfully Published!');
        return redirect()->route('admin.posts.edit', $post->id)->withInput();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $post = Post::where('post_type', 'post')->where('id', $id)->first();

        if (!isset($post->id))
            $post = new Post();

        return view('cms::post.post', compact('post'));
    }


    public function update(Request $request, $id)
    {
        // Update a post
        $post = $this->postRepo->update($request, $id);
        Flash::overlay('updated successfully');
        return redirect()->back()->withInput();
    }

    public function destroy($id)
    {
        $isDestroyed = $this->postRepo->destroy($id);

        if ($isDestroyed) {
            Flash::overlay('property deleted successfully');
            return redirect()->back();
        } else {
            Flash::overlay('Sorry unable to delete');
            return redirect()->back();
        }

    }

    /**
     * Select files for a single post.
     *
     * @param  Request $request
     * @return Response
     */
    public function postMedia(Request $request)
    {
        $postId = $request->get('post');
        if (!$postId) return redirect()->route('admin.root')->with('message', 'Seems like you have not selected a post.');

        $files = FileMedia::orderBy('created_at', 'DESC')->get();
        $selectType = 'single';
        return view('cms::post.post-files', compact('files', 'postId', 'selectType'));
    }

    public function storePostMedia(Request $request)
    {

        $post = $this->postRepo->addImage($request->get('post'), $request->get('file'));
        Flash::overlay('Image added  successfully');
        return redirect()->route('admin.posts.edit', $post->id);
    }

    public function postSubsidiaryMedia(Request $request)
    {
        $postId = $request->get('post');
        if (!$postId) return redirect()->route('admin.root')->with('message', 'Seems like you have not selected a post.');

        $files = FileMedia::orderBy('created_at', 'DESC')->get();
        $selectType = 'single';
        return view('cms::post.post-subsidiary-files', compact('files', 'postId', 'selectType'));
    }

    public function storeSubsidiaryPostMedia(Request $request)
    {
        if ($request->get('type')) {
            $post = $this->postSubsidiaryMediaRepo->addSubsidiaryImage($request->get('propertyId'), $request->get('files'));
            if ($post) {
                return ['status' => true, 'message' => 'media uploaded successfully'];
            } else {
                return ['status' => false, 'message' => 'error occurred while uploading media'];
            }
        } else {
            //var_dump($request->get('file'));dd();
            $post = $this->postSubsidiaryMediaRepo->addSubsidiaryImage($request->get('post'), $request->get('file'));
            if ($post) {
                Flash::overlay('image added successfully');
                return redirect()->route('admin.posts.edit', $request->get('post'));
            } else {
                Flash::overlay('sorry some error occurred');
                return redirect()->route('admin.posts.edit', $request->get('post'));

            }


        }

    }

    public function deleteSubsidiaryPostMedia(Request $request)
    {
        if ($request->get('imageId') && $request->get('propertyId')) {
            $isDestroyed = $this->postSubsidiaryMediaRepo->destroy($request->get('propertyId'), $request->get('imageId'));
            if($isDestroyed) {
                return ['status' => true, 'message' => 'media deleted successfully'];
            } else {
                return ['status' => false, 'message' => 'unable to delete'];

            }
        }
    }
}
