<?php

namespace Xadmin\Repositories;

use Xadmin\Models\Post;
use Xadmin\Models\PostSubsidiaryFiles;
use Xadmin\Models\FileMedia;

class PostSubsidiaryMediaRepo
{
    public function __construct()
    {

    }

    public function addSubsidiaryImage($postId, $fileMediaIdArray)
    {
        $sqlData = array();
        foreach ($fileMediaIdArray as $file) {
            $matchThese = ['post_id' => $postId, 'filemedia_id' => $file];
            $subsidiaryFiles = PostSubsidiaryFiles::where($matchThese);
            if ($subsidiaryFiles->count() == 0) {
                $sqlData[] = array(
                    'post_id' => $postId,
                    'filemedia_id' => $file
                );
            }
        }
        $result = PostSubsidiaryFiles::insert($sqlData);
        return $result;

    }

    public function destroy($propertyId, $mediaId)
    {
        $postSubsidiaryMedia = PostSubsidiaryFiles::where('post_id', $propertyId)->where('filemedia_id', $mediaId)->first();
        if ($postSubsidiaryMedia && $postSubsidiaryMedia->delete()) {
            return true;
        }

        return false;
    }
}