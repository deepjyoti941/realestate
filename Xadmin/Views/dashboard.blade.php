@extends('cms::layouts.default')

@section('content')
        <!-- Analytics Block -->
<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <button type="button"><i class="si si-settings"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i
                            class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="close"><i class="si si-close"></i>
                </button>
            </li>
        </ul>
        <h3 class="block-title">Analytics</h3>
    </div>
    <div class="block-content">
        <p>Will add some infographics </p>
    </div>
</div>
<!-- END Analytics Block -->

<!-- Shortcuts Block -->
<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <button type="button"><i class="si si-settings"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i
                            class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="close"><i class="si si-close"></i>
                </button>
            </li>
        </ul>
        <h3 class="block-title">Quick launch items</h3>
    </div>
    <div class="block-content">
        <p>Will add shortcuts to some action here</p>
    </div>
</div>
<!-- END Shortcuts Block -->

<!-- Enquiry Block -->
<div class="block">
    <div class="block-header">
        <ul class="block-options">
            <li>
                <button type="button"><i class="si si-settings"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i
                            class="si si-refresh"></i></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
            </li>
            <li>
                <button type="button" data-toggle="block-option" data-action="close"><i class="si si-close"></i>
                </button>
            </li>
        </ul>
        <h3 class="block-title">Customer enquiry analytics</h3>
    </div>
    <div class="block-content">
        <p>Monthly wise Customer enquiry report</p>
    </div>
</div>
<!-- END Enquiry Block -->
@stop