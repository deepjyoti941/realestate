@extends('cms::layouts.default')

@section('content')

    @include('cms::snippets.block-subsidiary-files-single')

@stop

@section('footer')
    <script src="{{ asset('xadmin/js/main.js') }}"></script>
@stop