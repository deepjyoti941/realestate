{{-- subsidiary Images --}}
<div class="block">
    <div class="block-header bg-gray-lighter">
        <ul class="block-options">
            <li>
                <a href="{{ route('admin.post-subsidiary-media.index', ['post'=>$post->id]) }}"><i
                            class="fa fa-plus"></i> Select
                    Image</a>
            </li>
        </ul>
        <h3 class="block-title">subsidiary images</h3>
    </div>
    <div class="block-content subsidiary-grid">
        <div class="row">
            @forelse($post->subsidiaryFiles as $file)
                <div class="col-md-4">
                    <img class="img-responsive" src="{{ $post->subsidiaryMediaImage($file->filename) }}" alt="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="text-center">
                        <button type="submit" class="btn btn-danger btn-xs btn-block btn-square remove-subsidiary-image"
                                name="remove_feature_image" value="{{ $file->id }}">Remove
                        </button>
                    </div>
                </div>
            @empty
                <div class="alert alert-warning">
                    <strong>No subsidiary images!</strong>
                </div>
            @endforelse

        </div>
    </div>
</div>

{{-- Feature Image End --}}