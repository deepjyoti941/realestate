<div class="block">
	<div class="block-header bg-gray-lighter">
        <h3 class="block-title">Files Gallery</h3>
        <button class="btn btn-default btn-add-selected pull-right">Upload selected files</button>
    </div>
	<div class="block-content">
		@if(count($files) > 0)
			<table class="table js-table-checkable tables-files">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 70px;">
                            <label class="css-input css-checkbox css-checkbox-primary remove-margin-t remove-margin-b">
                                <input type="checkbox" id="check-all" name="check-all"><span></span>
                            </label>
                        </th>
                    	<th>Preview</th>
                        <th>Filename</th>
                        <th class="hidden-xs" style="width: 15%;">Size</th>
                        <th class="text-center" style="width: 100px;">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($files as $key => $file)
                    	<tr>
                            <td class="text-center">
                                <label class="css-input css-checkbox css-checkbox-primary">
                                    <input type="checkbox" name="row_file" data-id="{{$file->id}}"><span></span>
                                </label>
                            </td>
                    		<td>{!! _fileMediaPreview($file) !!}</td>
                            <td>{{ $file->filename }}</td>
                            <td>{{ $file->size }}</td>
                            <td class="text-center">
                                <div class="btn-group">
                                     {!! Form::open([ 'method' => 'post', 'route' => 'admin.post-subsidiary-media.store']) !!}
                                        <input type="hidden" name="post" value="{{ $postId }}">
                                        <input type="hidden" name="file[]" value="{{ $file->id }}">
                                        <button class="btn btn-sm btn-default" type="submit">Add File</button>
                                    {!! Form::close() !!}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
		@else
			<div class="not-available text-center">
				No files available
			</div> 
		@endif
	</div>
</div>