<?php

namespace Xadmin\Models;

use Illuminate\Database\Eloquent\Model;

class PostSubsidiaryFiles extends Model
{
    protected $table = "post_subsidiary_files";
    protected $fillable = ['post_id', 'filemedia_id'];
    public $timestamps  = false;

}